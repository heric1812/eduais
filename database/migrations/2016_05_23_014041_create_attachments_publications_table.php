<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsPublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments_publications', function(Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('publication_id')->unsigned();
            $table->string('nombre_adjunto', 250)->nullable();
            $table->string('adjunto', 250)->nullable();
            $table->string('tipo_adjunto', 45)->nullable();

            $table->foreign('publication_id')
                ->references('id')->on('publications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachments_publications');
    }
}
