<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('email', 255)->unique();
            $table->string('password', 255);
            $table->string('avatar', 250)->nullable();
            $table->integer('cedula')->unique();
            $table->string('nombres', 255);
            $table->string('apellidos', 255);
            $table->enum('sexo', ['f','m']);
            $table->date('fecha_nacimiento');
            $table->string('profesion', 255)->nullable();
            $table->text('acerca_mi')->nullable();
            $table->text('que_hago')->nullable();
            $table->integer('nivel')->default(2);
            $table->integer('status')->default(1);
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
