<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            	
        // Array de los datos por defecto de las habilidades
        $skills = [
        	['type_skill_id'	=> '1',
		    'nombre_habilidad'	=> 'JavaScript'],
        	['type_skill_id'	=> '1',
		    'nombre_habilidad'	=> 'ECMAScript6'],
        	['type_skill_id' 	=> '1',
		    'nombre_habilidad'	=> 'Java'],
        	['type_skill_id'	=> '1',
		    'nombre_habilidad'	=> 'PHP'],
        	['type_skill_id'	=> '1',
		    'nombre_habilidad'	=> 'Python'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'C#'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'C++'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Ruby'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'HTML'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'CSS'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'C'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Objective-C'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Perl'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Shell'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'R'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Scala'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Go'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Haskell'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Matlab'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Swift'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Clojure'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Groovy'],
			['type_skill_id' 	=>  '1',
			'nombre_habilidad'	=> 'Visual Basic']

        ];

        foreach ($skills as $skill) {
        	\App\Skill::create($skill);
        }

    }
}
