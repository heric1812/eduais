<?php

use Illuminate\Database\Seeder;

class Type_skillsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
 
		// Array de los datos por defecto de un usuario
		$typeSkills = [
			['tipo_habilidad' => 'Programación'],
			['tipo_habilidad' => 'Ofimática'],
			['tipo_habilidad' => 'Redes']
		];

		foreach ($typeSkills as $typeSkill) {
			\App\TypeSkill::create($typeSkill);
		}

	}
}
