<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
    	
        // Array de los datos por defecto de un usuario
        $users = [
        	[
        		'name' => 'admin',
        		'email' => 'admin@gmail.com',
        		'password' => Hash::make('admin'),
                'avatar' => '',
                'cedula' => '12345678',
                'nombres' => 'Admin Nombre',
                'apellidos' => 'Admin Apellido',
                'sexo' => 'm',
                'fecha_nacimiento' => '2016-06-01',
                'profesion' => 'Ingeniero en Informática',
                'acerca_mi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id sunt, reprehenderit itaque veniam officia distinctio recusandae! Rerum labore esse facere, voluptatem maxime reprehenderit laudantium obcaecati libero aperiam, sed consequuntur vel.',
                'que_hago' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum rerum molestias pariatur, quo nisi cumque laborum, est vel placeat necessitatibus nostrum maxime at optio omnis. Architecto debitis obcaecati modi quos.',
                'nivel' => '1',
                'status' => '1'
        	]
        ];

        foreach ($users as $user) {
        	\App\User::create($user);
        }

    }
}