<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    
    use SoftDeletes;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'avatar', 
        'email', 
        'password', 
        'cedula', 
        'nombres', 
        'apellidos', 
        'sexo', 
        'fecha_nacimiento', 
        'profesion', 
        'acerca_mi', 
        'que_hago', 
        'nivel', 
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];
    /**
     * SoftDeletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /*
     * Relaciones
    */
    public function emails() {
        return $this->hasMany('App\Email')->orderBy('created_at', 'desc');
    }
    public function phones() {
        return $this->hasMany('App\Phone')->orderBy('created_at', 'desc');
    }
    public function lenguages() {
        return $this->hasMany('App\Lenguage')->orderBy('created_at', 'desc');
    }
    public function experiences() {
        return $this->hasMany('App\Experience')->orderBy('created_at', 'desc');
    }
    public function personal_skills() {
        return $this->hasMany('App\PersonalSkill')->orderBy('created_at', 'desc');
    }
    public function publications() {
        return $this->hasMany('App\Publication')->orderBy('created_at', 'desc');
    }
    public function comments() {
        return $this->hasMany('App\Comment')->orderBy('created_at', 'desc');
    }


}
