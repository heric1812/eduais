<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeSkill extends Model {

    protected $table = 'type_skills';

    protected $fillable = [
		'tipo_habilidad'
    ];

    /*
	 * Relaciones
    */

    public function skill() {
        return $this->hasMany('App\Skill');
    }

}
