<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {

    protected $table = 'skills';

    protected $fillable = [
		'type_skill_id',
		'nombre_habilidad'
    ];

    /*
	 * Relaciones
    */

    public function personal_skill() {
        return $this->hasOne('App\PersonalSkill');
    }
    public function type_skill() {
    	return $this->belongsTo('App\TypeSkill');
    }

}