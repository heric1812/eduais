<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publication extends Model {
    
    use SoftDeletes;
    
    protected $table = 'publications';

    protected $fillable = [
    	'user_id',
		'titulo',
		'informacion',
		'prioridad'
    ];

    protected $dates = ['deleted_at'];
    	
    /*
	 * Relaciones
    */

    public function user() {
    	return $this->belongsTo('App\User');
    }
    public function comments() {
        return $this->hasMany('App\Comment');
    }
    public function attachments_publications() {
        return $this->hasMany('App\AttachmentsPublication');
    }
}
