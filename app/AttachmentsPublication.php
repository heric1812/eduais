<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttachmentsPublication extends Model {
    
    protected $table = 'attachments_publications';

    protected $fillable = [
    	'publication_id',
		'adjunto',
		'tipo_adjunto'
    ];
    	
    /*
	 * Relaciones
    */

    public function publication() {
        return $this->belongsTo('App\Publication');
    }
}
