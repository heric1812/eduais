<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lenguage extends Model {
	
    protected $table = 'lenguages';

    /*
	 * Relaciones
    */
    public function user() {
    	return $this->belongsTo('App\User');
    }
    
}
