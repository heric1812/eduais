<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model {

	protected $table = 'phones';

    protected $fillable = [
        'user_id',
        'telefono'
    ];

    /*
	 * Relaciones
    */

    public function user() {
    	return $this->belongsTo('App\User');
    }
}
