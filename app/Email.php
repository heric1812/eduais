<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model {

	protected $table = 'emails';
  
    protected $fillable = [
        'user_id',
        'email'
    ];
    
    /*
	 * Relaciones
    */

    public function user() {
    	return $this->belongsTo('App\User');
    }

}
