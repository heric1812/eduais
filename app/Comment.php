<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model {
    
    use SoftDeletes;

	protected $table = 'comments';

    protected $fillable = [
        'user_id',
        'publication_id',
        'comentario'
    ];

    protected $dates = ['deleted_at'];
	
    /*
	 * Relaciones
    */

    public function user() {
    	return $this->belongsTo('App\User');
    }
    public function publication() {
    	return $this->belongsTo('App\Publication');
    }
}
