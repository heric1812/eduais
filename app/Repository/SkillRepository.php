<?php

namespace App\Repository;

use App\Skill;

class SkillRepository {











	public function listar() {
		// return Skill::all();
		$skill = Skill::all();

		return $skill->load(
			['personal_skill.users' => function($query) {
				$query->orderBy('created_at', 'desc');
			}],
			['type_skill' => function($query) {
				$query->orderBy('created_at', 'desc');
			}]
		);
	}
	










	public function obtener($id) {
		// return Skill::find($id);
		$skill = Skill::find($id);

		return $skill->load(
			['personal_skill.users' => function($query) {
				$query->orderBy('created_at', 'desc');
			}],
			['type_skill' => function($query) {
				$query->orderBy('created_at', 'desc');
			}]
		);
	}











	public function guardar($data) {

		$skill = new Skill();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$skill->exists = true; // Al colocar el exists en true se vuelve un update.
			$skill->id = $data['id'];
		}

		$skill->skill_id = $data['type_skill_id'];
		$skill->nombre_habilidad = $data['nombre_habilidad'];

		$skill->save();

	}











	public function eliminar($id) {
		Skill::destroy($id);
	}










}
