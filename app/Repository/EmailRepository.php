<?php

namespace App\Repository;

use App\Email;

class EmailRepository {










	public function listar() {
		// return Email::all();
		$email = Email::all();
		return $email->load(
			['user' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}
	










	public function obtener($id) {
		return Email::find($id);
	}











	public function guardar($data) {
		// Se renombra el modelo para usarlo de forma mas facil.
		$email = new Email();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$email->exists = true; // Al colocar el exists en true se vuelve un update.
			$email->id = $data['id'];
		}

		$email->user_id = $data['user_id'];
		$email->email = $data['email'];

		$email->save();
	}











	public function eliminar($id) {
		Email::destroy($id);
	}








	

}
