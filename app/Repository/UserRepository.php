<?php

namespace App\Repository;

use App\User;

class UserRepository {



	public function listar() {
		return User::all();
		// $user = User::all();
		// return $user->load('emails','phones');
	}






	
	public function obtener($id) {
		$user = User::find($id);
		// Se llaman los datos con los cuales se tiene una relación
		return $user->load(
			[
				'emails' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'phones' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'lenguages' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'experiences' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'personal_skills.skill.type_skill' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'publications.attachments_publications' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'publications.comments' => function($query) {
					$query->orderBy('created_at', 'desc');
				},
				'comments.publication' => function($query) {
					$query->orderBy('created_at', 'desc');
				}
			]
		);
	}








	public function guardar($data) {

		if ($data->file('avatar')) {
			$originalName = $data->file('avatar')->getClientOriginalName();
		
			$originalName = date('ymdhis-').strtolower(str_replace('-', '', $originalName));
	
			$data->file('avatar')
				 ->move('storage/avatares', $originalName);
		} else {
			$originalName = null;
		}

		$user = new User();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$user->exists = true; // Al colocar el exists en true se vuelve un update.
			$user->id = $data['id'];
		}

        $user->name = $data['name'];
        $user->avatar = $data['avatar'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->cedula = $data['cedula'];
        $user->nombres = $data['nombres'];
        $user->apellidos = $data['apellidos'];
        $user->sexo = $data['sexo'];
        $user->fecha_nacimiento = $data['fecha_nacimiento'];
        $user->profesion = $data['profesion'];
        $user->acerca_mi = $data['acerca_mi'];
        $user->que_hago = $data['que_hago'];
        $user->nivel = $data['nivel'];
        $user->status = $data['status'];

		$user->avatar = $originalName;

		$user->save();
	}










	public function eliminar($id) {
		User::destroy($id);
	}








	

}
