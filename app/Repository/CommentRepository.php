<?php

namespace App\Repository;

use App\Comment;

class CommentRepository {







	public function listar() {
		// return Comment::all();
		$comment = Comment::all();
		return $comment->load(
			[
				'user' => function($query) {
				    $query->orderBy('created_at', 'desc');
				},
				'publication.user' => function($query) {
				    $query->orderBy('created_at', 'desc');
				}
			]
		);
	}
	










	public function obtener($id) {
		return Comment::find($id);
	}









	public function guardar($data) {
		// Se renombra el modelo para usarlo de forma mas facil.
		$comment = new Comment();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$comment->exists = true; // Al colocar el exists en true se vuelve un update.
			$comment->id = $data['id'];
		}

		$comment->user_id = $data['user_id'];
		$comment->publication_id = $data['publication_id'];
		$comment->comentario = $data['comentario'];

		$comment->save();
	}










	public function eliminar($id) {
		Comment::destroy($id);
	}








	

}
