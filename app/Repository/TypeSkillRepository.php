<?php

namespace App\Repository;

use App\TypeSkill;

class TypeSkillRepository {











	public function listar() {
		// return TypeSkill::all();
		$type_skill = TypeSkill::all();

		return $type_skill->load(
			['skill' => function($query) {
				$query->orderBy('id', 'asc');
			}]
		);
	}
	










	public function obtener($id) {
		// return TypeSkill::find($id);
		$type_skill = TypeSkill::find($id);

		return $type_skill->load(
			['skill' => function($query) {
				$query->orderBy('created_at', 'desc');
			}]
		);
	}











	public function guardar($data) {

		$type_skill = new TypeSkill();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$type_skill->exists = true; // Al colocar el exists en true se vuelve un update.
			$type_skill->id = $data['id'];
		}

		$type_skill->tipo_habilidad = $data['tipo_habilidad'];

		$type_skill->save();

	}











	public function eliminar($id) {
		TypeSkill::destroy($id);
	}










}
