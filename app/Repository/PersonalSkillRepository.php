<?php

namespace App\Repository;

use App\PersonalSkill;

class PersonalSkillRepository {











	public function listar() {
		// return PersonalSkill::all();
		$personal_skill = PersonalSkill::all();

		return $personal_skill->load(
			['user' => function($query) {
				$query->orderBy('created_at', 'desc');
			}],
			['skill.type_skill' => function($query) {
				$query->orderBy('created_at', 'desc');
			}]
		);
	}
	










	public function obtener($id) {
		// return PersonalSkill::find($id);
		$personal_skill = PersonalSkill::find($id);

		return $personal_skill->load(
			['user' => function($query) {
				$query->orderBy('created_at', 'desc');
			}],
			['skill.type_skill' => function($query) {
				$query->orderBy('created_at', 'desc');
			}]
		);
	}











	public function guardar($data) {

		$personal_skill = new PersonalSkill();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$personal_skill->exists = true; // Al colocar el exists en true se vuelve un update.
			$personal_skill->id = $data['id'];
		}

		$personal_skill->user_id = $data['user_id'];
		$personal_skill->skill_id = $data['skill_id'];
		$personal_skill->porcentaje = $data['porcentaje'];

		$personal_skill->save();

	}











	public function eliminar($id) {
		PersonalSkill::destroy($id);
	}










}
