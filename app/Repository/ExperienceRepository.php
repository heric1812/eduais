<?php

namespace App\Repository;

use App\Experience;

class ExperienceRepository {











	public function listar() {
		return Experience::all();
	}
	










	public function obtener($id) {
		// return Experience::find($id);
		$experience = Experience::find($id);
		return $experience->load(
			['user' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}














	public function guardar($data) {
		// Se renombra el modelo para usarlo de forma mas facil.
		$experience = new Experience();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$experience->exists = true; // Al colocar el exists en true se vuelve un update.
			$experience->id = $data['id'];
		}

		$experience->user_id = $data['user_id'];
		$experience->tipo_experiencia = $data['tipo_experiencia'];
		$experience->nombre_lugar = $data['nombre_lugar'];
		$experience->fecha_inicio = $data['fecha_inicio'];
		$experience->fecha_fin = $data['fecha_fin'];
		$experience->ubicacion = $data['ubicacion'];
		$experience->descripcion = $data['descripcion'];

		$experience->save();
	}











	public function eliminar($id) {
		Experience::destroy($id);
	}











}
