<?php

namespace App\Repository;

use App\Phone;

class PhoneRepository {









	public function listar() {
		// return Phone::all();
		$phone = Phone::all();
		return $phone->load(
			['user' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}










	
	public function obtener($id) {
		// return Phone::find($id);
		$phone = Phone::find($id);
		return $phone->load(
			['user' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}














	public function guardar($data) {
		// Se renombra el modelo para usarlo de forma mas facil.
		$phone = new Phone();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$phone->exists = true; // Al colocar el exists en true se vuelve un update.
			$phone->id = $data['id'];
		}

		$phone->user_id = $data['user_id'];
		$phone->telefono = $data['telefono'];

		$phone->save();
	}













	public function eliminar($id) {
		Phone::destroy($id);
	}












}
