<?php

namespace App\Repository;

use App\Lenguage;

class LenguageRepository {
	








	public function listar() {
		// return Lenguage::all();
		$lenguage = Lenguage::all();
		return $lenguage->load(
			['user' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}











	public function obtener($id) {
		return Lenguage::find($id);
	}












	public function guardar($data) {
		// Se renombra el modelo para usarlo de forma mas facil.
		$lenguage = new Lenguage();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$lenguage->exists = true; // Al colocar el exists en true se vuelve un update.
			$lenguage->id = $data['id'];
		}

		$lenguage->user_id = $data['user_id'];
		$lenguage->lenguaje = $data['lenguaje'];
		$lenguage->nivel = $data['nivel'];

		$lenguage->save();
	}









	public function eliminar($id) {
		Lenguage::destroy($id);
	}









	

}
