<?php

namespace App\Repository;

use App\Publication,
	App\AttachmentsPublication;

class PublicationRepository {










	public function listar() {
		// return Publication::all();
		$publication = Publication::orderBy('created_at', 'DESC')
									->get();

		return $publication->load(
			[
				'user' => function($query) {
					$query->orderBy('created_at', 'DESC');
				},
				'attachments_publications' => function($query) {
					$query->orderBy('created_at', 'DESC');
				},
				'comments.user' => function($query) {
					$query->orderBy('created_at', 'DESC');
				}
			]
		);
	}









	
	public function obtener($id) {
		// return Publication::find($id);
		$publication = Publication::find($id);
		return $publication->load(
			[
				'user' => function($query) {
					$query->orderBy('created_at', 'DESC');
				},
				'attachments_publications' => function($query) {
					$query->orderBy('created_at', 'DESC');
				},
				'comments.user' => function($query) {
					$query->orderBy('created_at', 'DESC');
				}
			]
		);
	}













	// public function guardar($data) {
	// 	/*DB::transaction(function() {

	// 	});*/

	// 	$publication = new Publication();

	// 	// Especificar si es un UPDATE.
	// 	if ($data['id'] > 0) {
	// 		$publication->exists = true; // Al colocar el exists en true se vuelve un update.
	// 		$publication->id = $data['id'];
	// 	}

	// 	$publication->user_id = $data['user_id'];
	// 	$publication->titulo = $data['titulo'];
	// 	$publication->informacion = $data['informacion'];
	// 	$publication->prioridad = $data['prioridad'];

	// 	$publication->save();

	// }

	public function guardar($data) {
		$publication = new Publication();

		if ($data['id'] > 0) {
			$publication->exists = true;
			$publication->id = $data['id'];
		}

		$publication->user_id = $data['user_id'];
		$publication->titulo = $data['titulo'];
		$publication->informacion = $data['informacion'];
		$publication->prioridad = $data['prioridad'];
		$publication->materia = $data['materia'];

		$publication->save();

		if ($data->file('adjunto')) {
			
			$originalName = $data->file('adjunto')->getClientOriginalName();
			$originalExtension = $data->file('adjunto')->getClientOriginalExtension();
			$originalNameUnique = date('ymdhis-').strtolower(str_replace('-', '', $originalName));

			$attachment = new AttachmentsPublication();

			$attachment->publication_id = $publication->id;
			$attachment->nombre_adjunto = $data['nombre_adjunto'];
			$attachment->adjunto = $data['adjunto'];
			$attachment->tipo_adjunto = $data['tipo_adjunto'];

			$attachment->nombre_adjunto = $originalName;
			$attachment->adjunto = $originalNameUnique;
			$attachment->tipo_adjunto = $originalExtension;

			$attachment->save();

			$data->file('adjunto')
				 ->move('storage/adjuntos', $originalName);
		
		}

	}












	public function eliminar($id) {
		Publication::destroy($id);
	}









	

}
