<?php

namespace App\Repository;

use App\AttachmentsPublication;

class AttachmentsPublicationRepository {











	public function listar() {
		// return AttachmentsPublication::all();
		$attachment = AttachmentsPublication::all();

		return $attachment->load(
			['publication' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}
	










	public function obtener($id) {
		// return AttachmentsPublication::find($id);
		$attachment = AttachmentsPublication::find($id);

		return $attachment->load(
			['publication' => function($query) {
			    $query->orderBy('created_at', 'desc');
			}]
		);
	}











	public function guardar($data) {

		$originalName = $data->file('adjunto')->getClientOriginalName();
		$originalExtension = $data->file('adjunto')->getClientOriginalExtension();

		$originalName = date('ymdhis-').strtolower(str_replace('-', '', $originalName));

		$attachment = new AttachmentsPublication();

		// Especificar si es un UPDATE.
		if ($data['id'] > 0) {
			$attachment->exists = true; // Al colocar el exists en true se vuelve un update.
			$attachment->id = $data['id'];
		}

		$attachment->publication_id = $data['publication_id'];
		$attachment->adjunto = $data['adjunto'];
		$attachment->tipo_adjunto = $data['tipo_adjunto'];

		$attachment->adjunto = $originalName;
		$attachment->tipo_adjunto = $originalExtension;

		$attachment->save();

		$data->file('adjunto')
			 ->move('storage/adjuntos', $originalName);
	}











	public function eliminar($id) {
		AttachmentsPublication::destroy($id);
	}










}
