<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalSkill extends Model {
    
    protected $table = 'personal_skills';

    protected $fillable = [
    	'user_id',
		'skill_id',
		'porcentaje'
    ];

    /*
	 * Relaciones
    */

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function skill() {
    	return $this->belongsTo('App\Skill');
    }

}