<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

// Para las funciones de validacion
use Illuminate\Support\Facades\Validator;

// Llamada al modelo del repositorio
use App\Repository\PublicationRepository;

class PublicationController extends Controller {

	// Crear una variale global privada de los repositorios
	private $publicationRepo;

	// Injeccion del modelo en el contructor
	public function __construct(PublicationRepository $publicationRepo) {
		$this->middleware('cors');

		// Se renombra la variable para poder usarla en el controlador
		$this->publicationRepo = $publicationRepo;
	}





	/** FUNCIONA!!!!
		Lista todas las publicaciones en la base de datos.
	 */
	public function index() {
		// Se retorna una respuesta en formato JSON con todo el listado de las publicaciones
		return response()->json($this->publicationRepo->listar());
	}
	// Funcion solo para enviar a la vista los datos de las publicaciones con prioridad alta.
	public function prioridad() {
		return response()->json($this->publicationRepo->prioridad());
	}





	/** FUNCIONA!!!
		Se llama al mismo metodo tanto para GUARDAR como para MODIFICAR.
	 */
	public function store(Request $request) {
		// Se guarda todo el request en una variable para usar
		$data = $request->all();

		// Se nombran las reglas de validacion a usar
		$rules = array(
			'user_id' 	=> 'required|numeric', 
			'titulo' 	=> 'required', 
			'informacion' 	=> 'required'
		);

		// Se guarda en una variable la validacion
		$v = Validator::make($data, $rules);

		// Se valida
		if ($v->fails()) {
			// Se retorna una respuesta con los errores
			return response()->json(['error' => $v->errors()], 400);
		}





		// Se verifica si no hay errores
		try {
			// Comienza la transacción para validar el guardado de la información
			\DB::beginTransaction();
			
				// Se envian los datos al modelo del repositorio, llamando al metodo guardar
				$this->publicationRepo->guardar($request);

			// Se guarda la información ya que todo esta bien
			\DB::commit();

			// Se retorna una respuesta a la vista con un mensaje
			return response()->json(['mensaje'=>'publication_success'], 200);

		}
		// Cualquier excepción se enviara aquí y se elimina lo anterior guardado o no guardado y se envia un error
		catch(Exception $ex) {
			\DB::rollback();
			return response()->json(['error'=>'error_interval_server'], 500);
		}

	}







	/** FUNCIONA!!!
		Se visualiza la publicacion especifica a la que se desea ingresar.
	 */
	public function show($id) {
		// Se guarda en la variable el id evaluado en la condicion ternaria con el metodo obtener del modelo en el repositorio
		$this->publication = ($id > 0 ? $this->publicationRepo->obtener($id) : null);

		// Se envia la publicacion obtenida en formato JSON al cliente api
		return response()->json($this->publication);
	}







	/** FUNCIONA!!!
		Se llama a travez del protocolo DELETE para que pueda funcionar.
	 */
	public function destroy($id) {
		// Al obtener el id, se le envia a la funcion eliminar del modelo repositorio para ser eliminado
		$this->publicationRepo->eliminar($id);
		// Se envia una respuesta al cliente
		return response()->json(['mensaje' => 'delete_success'], 200);
	}










	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

}
