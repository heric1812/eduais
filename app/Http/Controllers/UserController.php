<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// Para las funciones de validacion
use Illuminate\Support\Facades\Validator;

// Se importa para poder pasar parametros por la ruta
// use Illuminate\Routing\Route;

// Llamada al modelo del repositorio
use App\Repository\UserRepository;

// Agregar los archivos de JWT (Json Web Token)
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller {

	// Crear una variale global privada de los repositorios
	// private $route;
	private $userRepo;

	// Injeccion del modelo en el contructor
	public function __construct(UserRepository $userRepo) {
		$this->middleware('cors');

		// Se renombra la variable para poder usarla en el controlador
		// $this->route = $route;
		$this->userRepo = $userRepo;
	}









	/** FUNCIONA!!!!
		Función de autenticación, agregando el Token, y autenticación por Google Plus
	 */
	public function userAuth(Request $request) {
		$credentials = $request->only('email', 'password');
		$token = null;
		$user = null;

		try {
			if (!$request->has('password')) {
				$user = User::where('email', $request->input('email'))->first();
				if (empty($user)) {
					$user = User::create([
						'name' => $request->input('name'),
						'email' => $request->input('email'),
						'avatar' => $request->input('avatar')
					]);

				}
				if (! $token = JWTAuth::fromUser($user) ) {
					return response()->json(['error' => 'invalid_credentials'], 500);
				}
			}else {
				// Irá al modelo confirmara si los datos son validos, en caso contrario
				// enviará el error de invalides.
				if (! $token = JWTAuth::attempt($credentials)) {
					return response()->json(['error' => 'invalid_credentials'], 500);
				}

				// En caso de que todo este bien, envia el token a la app
				$user = JWTAuth::toUser($token);
			}

		} catch (JWTException $e) {

			return response()->json(['error' => 'somthing_went_wrong'], 500);

		}

		return response()->json(compact('token', 'user'));

	}








	/** FUNCIONA!!!!
		Lista todas los usuarios en la base de datos.
	 */
	public function index() {
		// Se retorna una respuesta en formato JSON con todo el listado de los usuarios
		return response()->json($this->userRepo->listar());
	}








	/** FUNCIONA!!!
		Se llama al mismo metodo tanto para GUARDAR como para MODIFICAR.
	 */
	public function store(Request $request) {
		// Se guarda todo el request en una variable para usar
		$data = $request->all();

		try {
			\DB::beginTransaction();
			
				// Se nombran las reglas de validacion a usar
				$rules = array(
					'avatar' => 'image', 
					'email' => 'required|email|unique:users,email,'.$request->input('id'), 
					'password' => 'required', 
					'cedula' => 'required|numeric|unique:users,cedula,'.$request->input('id'), 
					'nombres'=> 'required', 
					'apellidos' => 'required', 
					'sexo' => 'required|in:m,f', 
					'fecha_nacimiento' => 'required|date' 
				);

				// Se guarda en una variable la validacion
				$v = Validator::make($data, $rules);

				// Se valida
				if ($v->fails()) {
					// Se retorna una respuesta con los errores
					return response()->json(['error' => $v->errors()], 500);
				}
				
				// Se envian los datos al modelo del repositorio, llamando al metodo guardar
				$this->userRepo->guardar($request);

			\DB::commit();

			// Se retorna una respuesta a la vista con un mensaje
			return response()->json(['mensaje'=>'user_success'], 200);
			
		} catch (Exception $e) {
			\DB::rollback();
			return response()->json(['error'=>'error_internal_server'], 500);
		}

	}








	/** FUNCIONA!!!
		Se visualiza la publicacion especifica a la que se desea ingresar.
	 */
	public function show($id) {
		// Se guarda en la variable el id evaluado en la condicion ternaria con el metodo obtener del modelo en el repositorio
		$this->user = ($id > 0 ? $this->userRepo->obtener($id) : null);

		// Se envia la publicacion obtenida en formato JSON al cliente api
		return response()->json($this->user);
	}










	/** FUNCIONA!!!
		Se llama a travez del protocolo DELETE para que pueda funcionar.
	 */
	public function destroy($id) {
		// Al obtener el id, se le envia a la funcion eliminar del modelo repositorio para ser eliminado
		$this->userRepo->eliminar($id);
		// Se envia una respuesta al cliente
		return response()->json(['mensaje' => 'delete_success'], 200);
	}












	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

}
