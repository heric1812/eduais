<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// Para las funciones de validacion
use Illuminate\Support\Facades\Validator;

// Llamada al modelo del repositorio
use App\Repository\AttachmentsPublicationRepository;

class AttachmentsPublicationController extends Controller {

	// Crear una variale global privada de los repositorios
	private $attachmentRepo;

	// Injeccion del modelo en el contructor
	public function __construct(AttachmentsPublicationRepository $attachmentRepo) {
		$this->middleware('cors');

		// Se renombra la variable para poder usarla en el controlador
		$this->attachmentRepo = $attachmentRepo;
	}





	/** FUNCIONA!!!!
		Lista todas las publicaciones en la base de datos.
	 */
	public function index() {
		// Se retorna una respuesta en formato JSON con todo el listado de las publicaciones
		return response()->json($this->attachmentRepo->listar());
	}






	/** FUNCIONA!!!
		Se llama al mismo metodo tanto para GUARDAR como para MODIFICAR.
	 */
	public function store(Request $request) {
		// Se guarda todo el request en una variable para usar
		$data = $request->all();

		// Se nombran las reglas de validacion a usar
		$rules = array(
			'publication_id' => 'required|numeric', 
			// 'adjunto' => 'required'
		);

		// Se guarda en una variable la validacion
		$v = Validator::make($data, $rules);

		// Se valida
		if ($v->fails()) {
			// Se retorna una respuesta con los errores
			return response()->json(['error' => $v->errors()]);
		}
		// Se envian los datos al modelo del repositorio, llamando al metodo guardar
		$this->attachmentRepo->guardar($request);

		// Se retorna una respuesta a la vista con un mensaje
		return response()->json(['mensaje'=>'attachment_success'], 200);
	}







	/** FUNCIONA!!!
		Se visualiza la publicacion especifica a la que se desea ingresar.
	 */
	public function show($id) {
		// Se guarda en la variable el id evaluado en la condicion ternaria con el metodo obtener del modelo en el repositorio
		$this->attachment = ($id > 0 ? $this->attachmentRepo->obtener($id) : null);

		// Se envia la publicacion obtenida en formato JSON al cliente api
		return response()->json($this->attachment);
	}







	/** FUNCIONA!!!
		Se llama a travez del protocolo DELETE para que pueda funcionar.
	 */
	public function destroy($id) {
		// Al obtener el id, se le envia a la funcion eliminar del modelo repositorio para ser eliminado
		$this->attachmentRepo->eliminar($id);
		// Se envia una respuesta al cliente
		return response()->json(['mensaje' => 'delete_success'], 200);
	}










	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

}
