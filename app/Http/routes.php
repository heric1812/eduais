<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'cors'], function() {

	// Al ingresar a la ruta de login se llama al controllador y a la funcion a realizar
	Route::post('/auth_login', 'UserController@userAuth');
	
	// Rutas por controladores implicitos, se genera la url automatica para la api
	Route::resource('person', 'UserController');
	Route::resource('email', 'EmailController');
	Route::resource('phone', 'PhoneController');
	Route::resource('lenguage', 'LenguageController');
	Route::resource('experience', 'ExperienceController');
	Route::resource('personalskill', 'PersonalSkillController');

	Route::resource('skill', 'SkillController');
	Route::resource('typeskill', 'TypeSkillController');

	Route::get('prioridad', 'PublicationController@prioridad');
	Route::resource('publication', 'PublicationController');
	Route::resource('attachment', 'AttachmentsPublicationController');
	Route::resource('comment', 'CommentController');

});
